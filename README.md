# Reindeer_Step_ZYBO

#### 介绍
将Reindeer_Step的开源RISC-V软核移植到ZYBO平台，并且ZYBO自带的ARM核和RISC-V软核共享内存。此外准备为Reindeer增加协处理器接口。最后希望可以在Linux环境下运行，实现一站式调用RISC-V软核。
Reindeer_Step开源仓库：https://github.com/PulseRain/Reindeer_Step.git

觉得不错就点亮星星吧！

#### 软件架构
软件：vivado 2019.1
开发板：ZYBO 
基于：Reindeer_Step开源仓库：https://github.com/PulseRain/Reindeer_Step.git


#### 安装与使用说明

1.  git clone https://gitee.com/yuzihao1231/reindeer_step_zybo.git
2.  使用vivado 2019.1打开即可
3.  综合生成bit流
4.  打开SDK，运行C

如何生成内存数据？
请参考：《基于RISCV与FPGA的嵌入式系统开发》，或博客https://blog.csdn.net/qq_45467083/article/details/120906357
这里提供一个讲原来的dat文件转换为0x格式的C代码，在 ./参考资料/DDRData.c 里面。

#### 实现功能与贡献者
1. arm与riscv共享内存，riscv移植成功;
    当前状态：已完成;
    贡献者：Yu Zihao;
2. arm控制riscv启动与阻塞;
    当前状态：已完成;
    贡献者：Yu Zihao;
3. riscv添加协处理器接口;
    当前状态：感觉修改这个意义不大，我还是按照自己的需求重新写一个软核吧;
    贡献者：;
4. arm端使用linux，实现在Linux环境下编写c，编译，然后直接烧录进入内存，启动riscv;
    当前状态：未完成，这个在参考资料里面记录了完备的解决方案;
    贡献者：;

#### 联系方式
邮箱：yzh1632@163.com
我的博客：https://blog.csdn.net/qq_45467083/article/details/121267866

欢迎加入到RISCV学习大家庭，以及这一工程的实现中。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
