set_property SRC_FILE_INFO {cfile:d:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_processing_system7_0_0/system_top_processing_system7_0_0.xdc rfile:../Reindeer.srcs/sources_1/bd/system_top/ip/system_top_processing_system7_0_0/system_top_processing_system7_0_0.xdc id:1 order:EARLY scoped_inst:system_top_i/processing_system7_0/inst} [current_design]
set_property SRC_FILE_INFO {cfile:D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/constrs_1/new/ZYBO.xdc rfile:../Reindeer.srcs/constrs_1/new/ZYBO.xdc id:2} [current_design]
current_instance system_top_i/processing_system7_0/inst
set_property src_info {type:SCOPED_XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter clk_fpga_0 0.6
current_instance
set_property src_info {type:XDC file:2 line:12 export:INPUT save:INPUT read:READ} [current_design]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:2 line:15 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port dbg_hub/clk [get_nets clk]
