// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Nov 23 16:08:16 2021
// Host        : DESKTOP-65CBDF8 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_axi_ddr_controller_0_0/system_top_axi_ddr_controller_0_0_sim_netlist.v
// Design      : system_top_axi_ddr_controller_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_top_axi_ddr_controller_0_0,axi_ddr_controller,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "axi_ddr_controller,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module system_top_axi_ddr_controller_0_0
   (dram_mem_read_en,
    dram_mem_write_en,
    mem_byteenable,
    mem_addr,
    mem_write_data,
    mem_ack,
    mem_read_data,
    M_AXI_ACLK,
    M_AXI_ARESETN,
    M_AXI_AWID,
    M_AXI_AWADDR,
    M_AXI_AWLEN,
    M_AXI_AWSIZE,
    M_AXI_AWBURST,
    M_AXI_AWLOCK,
    M_AXI_AWCACHE,
    M_AXI_AWPROT,
    M_AXI_AWQOS,
    M_AXI_AWUSER,
    M_AXI_AWVALID,
    M_AXI_AWREADY,
    M_AXI_WDATA,
    M_AXI_WSTRB,
    M_AXI_WLAST,
    M_AXI_WUSER,
    M_AXI_WVALID,
    M_AXI_WREADY,
    M_AXI_BID,
    M_AXI_BRESP,
    M_AXI_BUSER,
    M_AXI_BVALID,
    M_AXI_BREADY,
    M_AXI_ARID,
    M_AXI_ARADDR,
    M_AXI_ARLEN,
    M_AXI_ARSIZE,
    M_AXI_ARBURST,
    M_AXI_ARLOCK,
    M_AXI_ARCACHE,
    M_AXI_ARPROT,
    M_AXI_ARQOS,
    M_AXI_ARUSER,
    M_AXI_ARVALID,
    M_AXI_ARREADY,
    M_AXI_RID,
    M_AXI_RDATA,
    M_AXI_RRESP,
    M_AXI_RLAST,
    M_AXI_RUSER,
    M_AXI_RVALID,
    M_AXI_RREADY);
  input dram_mem_read_en;
  input dram_mem_write_en;
  input [3:0]mem_byteenable;
  input [20:0]mem_addr;
  input [31:0]mem_write_data;
  output mem_ack;
  output [31:0]mem_read_data;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M_AXI_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXI_ACLK, ASSOCIATED_BUSIF M_AXI, ASSOCIATED_RESET M_AXI_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN system_top_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input M_AXI_ACLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 M_AXI_ARESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXI_ARESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input M_AXI_ARESETN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWID" *) output [0:0]M_AXI_AWID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWADDR" *) output [31:0]M_AXI_AWADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWLEN" *) output [7:0]M_AXI_AWLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWSIZE" *) output [2:0]M_AXI_AWSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWBURST" *) output [1:0]M_AXI_AWBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWLOCK" *) output M_AXI_AWLOCK;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWCACHE" *) output [3:0]M_AXI_AWCACHE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWPROT" *) output [2:0]M_AXI_AWPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWQOS" *) output [3:0]M_AXI_AWQOS;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWUSER" *) output [0:0]M_AXI_AWUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWVALID" *) output M_AXI_AWVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWREADY" *) input M_AXI_AWREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WDATA" *) output [31:0]M_AXI_WDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WSTRB" *) output [3:0]M_AXI_WSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WLAST" *) output M_AXI_WLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WUSER" *) output [3:0]M_AXI_WUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WVALID" *) output M_AXI_WVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WREADY" *) input M_AXI_WREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BID" *) input [0:0]M_AXI_BID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BRESP" *) input [1:0]M_AXI_BRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BUSER" *) input [0:0]M_AXI_BUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BVALID" *) input M_AXI_BVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BREADY" *) output M_AXI_BREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARID" *) output [0:0]M_AXI_ARID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARADDR" *) output [31:0]M_AXI_ARADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARLEN" *) output [7:0]M_AXI_ARLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARSIZE" *) output [2:0]M_AXI_ARSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARBURST" *) output [1:0]M_AXI_ARBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARLOCK" *) output M_AXI_ARLOCK;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARCACHE" *) output [3:0]M_AXI_ARCACHE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARPROT" *) output [2:0]M_AXI_ARPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARQOS" *) output [3:0]M_AXI_ARQOS;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARUSER" *) output [0:0]M_AXI_ARUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARVALID" *) output M_AXI_ARVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARREADY" *) input M_AXI_ARREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RID" *) input [0:0]M_AXI_RID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RDATA" *) input [31:0]M_AXI_RDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RRESP" *) input [1:0]M_AXI_RRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RLAST" *) input M_AXI_RLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RUSER" *) input [3:0]M_AXI_RUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RVALID" *) input M_AXI_RVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXI, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 50000000, ID_WIDTH 1, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 4, RUSER_WIDTH 4, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN system_top_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output M_AXI_RREADY;

  wire \<const0> ;
  wire \<const1> ;
  wire M_AXI_ACLK;
  wire [23:2]\^M_AXI_ARADDR ;
  wire M_AXI_ARESETN;
  wire M_AXI_ARREADY;
  wire M_AXI_ARVALID;
  wire [23:2]\^M_AXI_AWADDR ;
  wire M_AXI_AWREADY;
  wire M_AXI_AWVALID;
  wire M_AXI_BREADY;
  wire M_AXI_BVALID;
  wire [31:0]M_AXI_RDATA;
  wire M_AXI_RLAST;
  wire M_AXI_RREADY;
  wire M_AXI_RVALID;
  wire [31:0]M_AXI_WDATA;
  wire M_AXI_WLAST;
  wire M_AXI_WREADY;
  wire [3:0]M_AXI_WSTRB;
  wire M_AXI_WVALID;
  wire dram_mem_read_en;
  wire dram_mem_write_en;
  wire mem_ack;
  wire [20:0]mem_addr;
  wire [3:0]mem_byteenable;
  wire [31:0]mem_read_data;
  wire [31:0]mem_write_data;

  assign M_AXI_ARADDR[31] = \<const0> ;
  assign M_AXI_ARADDR[30] = \<const0> ;
  assign M_AXI_ARADDR[29] = \<const0> ;
  assign M_AXI_ARADDR[28] = \<const1> ;
  assign M_AXI_ARADDR[27] = \<const1> ;
  assign M_AXI_ARADDR[26] = \<const1> ;
  assign M_AXI_ARADDR[25] = \<const1> ;
  assign M_AXI_ARADDR[24] = \<const1> ;
  assign M_AXI_ARADDR[23:2] = \^M_AXI_ARADDR [23:2];
  assign M_AXI_ARADDR[1] = \<const0> ;
  assign M_AXI_ARADDR[0] = \<const0> ;
  assign M_AXI_ARBURST[1] = \<const0> ;
  assign M_AXI_ARBURST[0] = \<const1> ;
  assign M_AXI_ARCACHE[3] = \<const0> ;
  assign M_AXI_ARCACHE[2] = \<const0> ;
  assign M_AXI_ARCACHE[1] = \<const1> ;
  assign M_AXI_ARCACHE[0] = \<const0> ;
  assign M_AXI_ARID[0] = \<const0> ;
  assign M_AXI_ARLEN[7] = \<const0> ;
  assign M_AXI_ARLEN[6] = \<const0> ;
  assign M_AXI_ARLEN[5] = \<const0> ;
  assign M_AXI_ARLEN[4] = \<const0> ;
  assign M_AXI_ARLEN[3] = \<const0> ;
  assign M_AXI_ARLEN[2] = \<const0> ;
  assign M_AXI_ARLEN[1] = \<const0> ;
  assign M_AXI_ARLEN[0] = \<const0> ;
  assign M_AXI_ARLOCK = \<const0> ;
  assign M_AXI_ARPROT[2] = \<const0> ;
  assign M_AXI_ARPROT[1] = \<const0> ;
  assign M_AXI_ARPROT[0] = \<const0> ;
  assign M_AXI_ARQOS[3] = \<const0> ;
  assign M_AXI_ARQOS[2] = \<const0> ;
  assign M_AXI_ARQOS[1] = \<const0> ;
  assign M_AXI_ARQOS[0] = \<const0> ;
  assign M_AXI_ARSIZE[2] = \<const0> ;
  assign M_AXI_ARSIZE[1] = \<const1> ;
  assign M_AXI_ARSIZE[0] = \<const0> ;
  assign M_AXI_ARUSER[0] = \<const1> ;
  assign M_AXI_AWADDR[31] = \<const0> ;
  assign M_AXI_AWADDR[30] = \<const0> ;
  assign M_AXI_AWADDR[29] = \<const0> ;
  assign M_AXI_AWADDR[28] = \<const1> ;
  assign M_AXI_AWADDR[27] = \<const1> ;
  assign M_AXI_AWADDR[26] = \<const1> ;
  assign M_AXI_AWADDR[25] = \<const1> ;
  assign M_AXI_AWADDR[24] = \<const1> ;
  assign M_AXI_AWADDR[23:2] = \^M_AXI_AWADDR [23:2];
  assign M_AXI_AWADDR[1] = \<const0> ;
  assign M_AXI_AWADDR[0] = \<const0> ;
  assign M_AXI_AWBURST[1] = \<const0> ;
  assign M_AXI_AWBURST[0] = \<const1> ;
  assign M_AXI_AWCACHE[3] = \<const0> ;
  assign M_AXI_AWCACHE[2] = \<const0> ;
  assign M_AXI_AWCACHE[1] = \<const1> ;
  assign M_AXI_AWCACHE[0] = \<const0> ;
  assign M_AXI_AWID[0] = \<const0> ;
  assign M_AXI_AWLEN[7] = \<const0> ;
  assign M_AXI_AWLEN[6] = \<const0> ;
  assign M_AXI_AWLEN[5] = \<const0> ;
  assign M_AXI_AWLEN[4] = \<const0> ;
  assign M_AXI_AWLEN[3] = \<const0> ;
  assign M_AXI_AWLEN[2] = \<const0> ;
  assign M_AXI_AWLEN[1] = \<const0> ;
  assign M_AXI_AWLEN[0] = \<const0> ;
  assign M_AXI_AWLOCK = \<const0> ;
  assign M_AXI_AWPROT[2] = \<const0> ;
  assign M_AXI_AWPROT[1] = \<const0> ;
  assign M_AXI_AWPROT[0] = \<const0> ;
  assign M_AXI_AWQOS[3] = \<const0> ;
  assign M_AXI_AWQOS[2] = \<const0> ;
  assign M_AXI_AWQOS[1] = \<const0> ;
  assign M_AXI_AWQOS[0] = \<const0> ;
  assign M_AXI_AWSIZE[2] = \<const0> ;
  assign M_AXI_AWSIZE[1] = \<const1> ;
  assign M_AXI_AWSIZE[0] = \<const0> ;
  assign M_AXI_AWUSER[0] = \<const1> ;
  assign M_AXI_WUSER[3] = \<const0> ;
  assign M_AXI_WUSER[2] = \<const0> ;
  assign M_AXI_WUSER[1] = \<const0> ;
  assign M_AXI_WUSER[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  system_top_axi_ddr_controller_0_0_axi_ddr_controller inst
       (.M_AXI_ACLK(M_AXI_ACLK),
        .M_AXI_ARADDR(\^M_AXI_ARADDR ),
        .M_AXI_ARESETN(M_AXI_ARESETN),
        .M_AXI_ARREADY(M_AXI_ARREADY),
        .M_AXI_ARVALID(M_AXI_ARVALID),
        .M_AXI_AWADDR(\^M_AXI_AWADDR ),
        .M_AXI_AWREADY(M_AXI_AWREADY),
        .M_AXI_AWVALID(M_AXI_AWVALID),
        .M_AXI_BREADY(M_AXI_BREADY),
        .M_AXI_BVALID(M_AXI_BVALID),
        .M_AXI_RDATA(M_AXI_RDATA),
        .M_AXI_RLAST(M_AXI_RLAST),
        .M_AXI_RREADY(M_AXI_RREADY),
        .M_AXI_RVALID(M_AXI_RVALID),
        .M_AXI_WDATA(M_AXI_WDATA),
        .M_AXI_WLAST(M_AXI_WLAST),
        .M_AXI_WREADY(M_AXI_WREADY),
        .M_AXI_WSTRB(M_AXI_WSTRB),
        .M_AXI_WVALID(M_AXI_WVALID),
        .dram_mem_read_en(dram_mem_read_en),
        .dram_mem_write_en(dram_mem_write_en),
        .mem_ack(mem_ack),
        .mem_addr(mem_addr),
        .mem_byteenable(mem_byteenable),
        .mem_read_data(mem_read_data),
        .mem_write_data(mem_write_data));
endmodule

(* ORIG_REF_NAME = "axi_ddr_controller" *) 
module system_top_axi_ddr_controller_0_0_axi_ddr_controller
   (M_AXI_WVALID,
    M_AXI_RREADY,
    M_AXI_WLAST,
    mem_read_data,
    M_AXI_AWADDR,
    M_AXI_WDATA,
    M_AXI_WSTRB,
    M_AXI_ARADDR,
    mem_ack,
    M_AXI_BREADY,
    M_AXI_AWVALID,
    M_AXI_ARVALID,
    M_AXI_WREADY,
    M_AXI_RVALID,
    M_AXI_ACLK,
    M_AXI_ARESETN,
    M_AXI_RDATA,
    mem_addr,
    mem_write_data,
    mem_byteenable,
    dram_mem_write_en,
    dram_mem_read_en,
    M_AXI_RLAST,
    M_AXI_BVALID,
    M_AXI_AWREADY,
    M_AXI_ARREADY);
  output M_AXI_WVALID;
  output M_AXI_RREADY;
  output M_AXI_WLAST;
  output [31:0]mem_read_data;
  output [21:0]M_AXI_AWADDR;
  output [31:0]M_AXI_WDATA;
  output [3:0]M_AXI_WSTRB;
  output [21:0]M_AXI_ARADDR;
  output mem_ack;
  output M_AXI_BREADY;
  output M_AXI_AWVALID;
  output M_AXI_ARVALID;
  input M_AXI_WREADY;
  input M_AXI_RVALID;
  input M_AXI_ACLK;
  input M_AXI_ARESETN;
  input [31:0]M_AXI_RDATA;
  input [20:0]mem_addr;
  input [31:0]mem_write_data;
  input [3:0]mem_byteenable;
  input dram_mem_write_en;
  input dram_mem_read_en;
  input M_AXI_RLAST;
  input M_AXI_BVALID;
  input M_AXI_AWREADY;
  input M_AXI_ARREADY;

  wire M_AXI_ACLK;
  wire [21:0]M_AXI_ARADDR;
  wire \M_AXI_ARADDR[19]_INST_0_i_1_n_0 ;
  wire \M_AXI_ARADDR[19]_INST_0_i_2_n_0 ;
  wire \M_AXI_ARADDR[19]_INST_0_i_3_n_0 ;
  wire \M_AXI_ARADDR[19]_INST_0_n_0 ;
  wire \M_AXI_ARADDR[19]_INST_0_n_1 ;
  wire \M_AXI_ARADDR[19]_INST_0_n_2 ;
  wire \M_AXI_ARADDR[19]_INST_0_n_3 ;
  wire M_AXI_ARESETN;
  wire M_AXI_ARREADY;
  wire M_AXI_ARVALID;
  wire [21:0]M_AXI_AWADDR;
  wire \M_AXI_AWADDR[19]_INST_0_i_1_n_0 ;
  wire \M_AXI_AWADDR[19]_INST_0_i_2_n_0 ;
  wire \M_AXI_AWADDR[19]_INST_0_i_3_n_0 ;
  wire \M_AXI_AWADDR[19]_INST_0_n_0 ;
  wire \M_AXI_AWADDR[19]_INST_0_n_1 ;
  wire \M_AXI_AWADDR[19]_INST_0_n_2 ;
  wire \M_AXI_AWADDR[19]_INST_0_n_3 ;
  wire M_AXI_AWREADY;
  wire M_AXI_AWVALID;
  wire M_AXI_BREADY;
  wire M_AXI_BVALID;
  wire [31:0]M_AXI_RDATA;
  wire M_AXI_RLAST;
  wire M_AXI_RREADY;
  wire M_AXI_RVALID;
  wire [31:0]M_AXI_WDATA;
  wire M_AXI_WLAST;
  wire M_AXI_WREADY;
  wire [3:0]M_AXI_WSTRB;
  wire M_AXI_WVALID;
  wire [22:19]axi_araddr;
  wire axi_arvalid_i_1_n_0;
  wire [22:19]axi_awaddr;
  wire axi_awvalid_i_1_n_0;
  wire axi_bready_i_1_n_0;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire axi_rdata_ready_reg_n_0;
  wire axi_rready_i_1_n_0;
  wire \axi_wdata[31]_i_1_n_0 ;
  wire axi_wdata_ready_reg_n_0;
  wire axi_wvalid_i_1_n_0;
  wire dram_mem_read_en;
  wire dram_mem_write_en;
  wire mem_ack;
  wire [20:0]mem_addr;
  wire \mem_addr_reg_reg_n_0_[10] ;
  wire \mem_addr_reg_reg_n_0_[11] ;
  wire \mem_addr_reg_reg_n_0_[12] ;
  wire \mem_addr_reg_reg_n_0_[13] ;
  wire \mem_addr_reg_reg_n_0_[14] ;
  wire \mem_addr_reg_reg_n_0_[15] ;
  wire \mem_addr_reg_reg_n_0_[16] ;
  wire \mem_addr_reg_reg_n_0_[17] ;
  wire \mem_addr_reg_reg_n_0_[18] ;
  wire \mem_addr_reg_reg_n_0_[19] ;
  wire \mem_addr_reg_reg_n_0_[20] ;
  wire \mem_addr_reg_reg_n_0_[21] ;
  wire \mem_addr_reg_reg_n_0_[22] ;
  wire \mem_addr_reg_reg_n_0_[2] ;
  wire \mem_addr_reg_reg_n_0_[3] ;
  wire \mem_addr_reg_reg_n_0_[4] ;
  wire \mem_addr_reg_reg_n_0_[5] ;
  wire \mem_addr_reg_reg_n_0_[6] ;
  wire \mem_addr_reg_reg_n_0_[7] ;
  wire \mem_addr_reg_reg_n_0_[8] ;
  wire \mem_addr_reg_reg_n_0_[9] ;
  wire [3:0]mem_byteenable;
  wire mem_byteenable_reg;
  wire [31:0]mem_read_data;
  wire [31:0]mem_write_data;
  wire [31:0]mem_write_data_reg;
  wire r_pluse_sync_i_1_n_0;
  wire r_pluse_sync_reg_n_0;
  wire w_pluse_sync_i_1_n_0;
  wire w_pluse_sync_reg_n_0;
  wire [3:1]\NLW_M_AXI_ARADDR[23]_INST_0_CO_UNCONNECTED ;
  wire [3:0]\NLW_M_AXI_ARADDR[23]_INST_0_O_UNCONNECTED ;
  wire [3:1]\NLW_M_AXI_AWADDR[23]_INST_0_CO_UNCONNECTED ;
  wire [3:0]\NLW_M_AXI_AWADDR[23]_INST_0_O_UNCONNECTED ;

  CARRY4 \M_AXI_ARADDR[19]_INST_0 
       (.CI(1'b0),
        .CO({\M_AXI_ARADDR[19]_INST_0_n_0 ,\M_AXI_ARADDR[19]_INST_0_n_1 ,\M_AXI_ARADDR[19]_INST_0_n_2 ,\M_AXI_ARADDR[19]_INST_0_n_3 }),
        .CYINIT(1'b0),
        .DI({axi_araddr[22:20],1'b0}),
        .O(M_AXI_ARADDR[20:17]),
        .S({\M_AXI_ARADDR[19]_INST_0_i_1_n_0 ,\M_AXI_ARADDR[19]_INST_0_i_2_n_0 ,\M_AXI_ARADDR[19]_INST_0_i_3_n_0 ,axi_araddr[19]}));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_ARADDR[19]_INST_0_i_1 
       (.I0(axi_araddr[22]),
        .O(\M_AXI_ARADDR[19]_INST_0_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_ARADDR[19]_INST_0_i_2 
       (.I0(axi_araddr[21]),
        .O(\M_AXI_ARADDR[19]_INST_0_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_ARADDR[19]_INST_0_i_3 
       (.I0(axi_araddr[20]),
        .O(\M_AXI_ARADDR[19]_INST_0_i_3_n_0 ));
  CARRY4 \M_AXI_ARADDR[23]_INST_0 
       (.CI(\M_AXI_ARADDR[19]_INST_0_n_0 ),
        .CO({\NLW_M_AXI_ARADDR[23]_INST_0_CO_UNCONNECTED [3:1],M_AXI_ARADDR[21]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_M_AXI_ARADDR[23]_INST_0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \M_AXI_AWADDR[19]_INST_0 
       (.CI(1'b0),
        .CO({\M_AXI_AWADDR[19]_INST_0_n_0 ,\M_AXI_AWADDR[19]_INST_0_n_1 ,\M_AXI_AWADDR[19]_INST_0_n_2 ,\M_AXI_AWADDR[19]_INST_0_n_3 }),
        .CYINIT(1'b0),
        .DI({axi_awaddr[22:20],1'b0}),
        .O(M_AXI_AWADDR[20:17]),
        .S({\M_AXI_AWADDR[19]_INST_0_i_1_n_0 ,\M_AXI_AWADDR[19]_INST_0_i_2_n_0 ,\M_AXI_AWADDR[19]_INST_0_i_3_n_0 ,axi_awaddr[19]}));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_AWADDR[19]_INST_0_i_1 
       (.I0(axi_awaddr[22]),
        .O(\M_AXI_AWADDR[19]_INST_0_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_AWADDR[19]_INST_0_i_2 
       (.I0(axi_awaddr[21]),
        .O(\M_AXI_AWADDR[19]_INST_0_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \M_AXI_AWADDR[19]_INST_0_i_3 
       (.I0(axi_awaddr[20]),
        .O(\M_AXI_AWADDR[19]_INST_0_i_3_n_0 ));
  CARRY4 \M_AXI_AWADDR[23]_INST_0 
       (.CI(\M_AXI_AWADDR[19]_INST_0_n_0 ),
        .CO({\NLW_M_AXI_AWADDR[23]_INST_0_CO_UNCONNECTED [3:1],M_AXI_AWADDR[21]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_M_AXI_AWADDR[23]_INST_0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  FDRE \axi_araddr_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[10] ),
        .Q(M_AXI_ARADDR[8]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[11] ),
        .Q(M_AXI_ARADDR[9]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[12] ),
        .Q(M_AXI_ARADDR[10]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[13] ),
        .Q(M_AXI_ARADDR[11]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[14] ),
        .Q(M_AXI_ARADDR[12]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[15] ),
        .Q(M_AXI_ARADDR[13]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[16] ),
        .Q(M_AXI_ARADDR[14]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[17] ),
        .Q(M_AXI_ARADDR[15]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[18] ),
        .Q(M_AXI_ARADDR[16]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[19] ),
        .Q(axi_araddr[19]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[20] ),
        .Q(axi_araddr[20]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[21] ),
        .Q(axi_araddr[21]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[22] ),
        .Q(axi_araddr[22]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[2] ),
        .Q(M_AXI_ARADDR[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[3] ),
        .Q(M_AXI_ARADDR[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[4] ),
        .Q(M_AXI_ARADDR[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[5] ),
        .Q(M_AXI_ARADDR[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[6] ),
        .Q(M_AXI_ARADDR[4]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[7] ),
        .Q(M_AXI_ARADDR[5]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[8] ),
        .Q(M_AXI_ARADDR[6]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_araddr_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(r_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[9] ),
        .Q(M_AXI_ARADDR[7]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h3A)) 
    axi_arvalid_i_1
       (.I0(r_pluse_sync_reg_n_0),
        .I1(M_AXI_ARREADY),
        .I2(M_AXI_ARVALID),
        .O(axi_arvalid_i_1_n_0));
  FDRE axi_arvalid_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_arvalid_i_1_n_0),
        .Q(M_AXI_ARVALID),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[10] ),
        .Q(M_AXI_AWADDR[8]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[11] ),
        .Q(M_AXI_AWADDR[9]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[12] ),
        .Q(M_AXI_AWADDR[10]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[13] ),
        .Q(M_AXI_AWADDR[11]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[14] ),
        .Q(M_AXI_AWADDR[12]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[15] ),
        .Q(M_AXI_AWADDR[13]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[16] ),
        .Q(M_AXI_AWADDR[14]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[17] ),
        .Q(M_AXI_AWADDR[15]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[18] ),
        .Q(M_AXI_AWADDR[16]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[19] ),
        .Q(axi_awaddr[19]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[20] ),
        .Q(axi_awaddr[20]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[21] ),
        .Q(axi_awaddr[21]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[22] ),
        .Q(axi_awaddr[22]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[2] ),
        .Q(M_AXI_AWADDR[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[3] ),
        .Q(M_AXI_AWADDR[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[4] ),
        .Q(M_AXI_AWADDR[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[5] ),
        .Q(M_AXI_AWADDR[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[6] ),
        .Q(M_AXI_AWADDR[4]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[7] ),
        .Q(M_AXI_AWADDR[5]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[8] ),
        .Q(M_AXI_AWADDR[6]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(w_pluse_sync_reg_n_0),
        .D(\mem_addr_reg_reg_n_0_[9] ),
        .Q(M_AXI_AWADDR[7]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h3A)) 
    axi_awvalid_i_1
       (.I0(w_pluse_sync_reg_n_0),
        .I1(M_AXI_AWREADY),
        .I2(M_AXI_AWVALID),
        .O(axi_awvalid_i_1_n_0));
  FDRE axi_awvalid_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_awvalid_i_1_n_0),
        .Q(M_AXI_AWVALID),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0040)) 
    axi_bready_i_1
       (.I0(M_AXI_BREADY),
        .I1(M_AXI_BVALID),
        .I2(M_AXI_ARESETN),
        .I3(w_pluse_sync_reg_n_0),
        .O(axi_bready_i_1_n_0));
  FDRE axi_bready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_bready_i_1_n_0),
        .Q(M_AXI_BREADY),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \axi_rdata[31]_i_1 
       (.I0(M_AXI_ARESETN),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_2 
       (.I0(M_AXI_RVALID),
        .I1(M_AXI_RREADY),
        .I2(axi_rdata_ready_reg_n_0),
        .O(\axi_rdata[31]_i_2_n_0 ));
  FDRE axi_rdata_ready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_rdata[31]_i_2_n_0 ),
        .Q(axi_rdata_ready_reg_n_0),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[0]),
        .Q(mem_read_data[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[10]),
        .Q(mem_read_data[10]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[11]),
        .Q(mem_read_data[11]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[12]),
        .Q(mem_read_data[12]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[13]),
        .Q(mem_read_data[13]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[14]),
        .Q(mem_read_data[14]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[15]),
        .Q(mem_read_data[15]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[16]),
        .Q(mem_read_data[16]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[17]),
        .Q(mem_read_data[17]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[18]),
        .Q(mem_read_data[18]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[19]),
        .Q(mem_read_data[19]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[1]),
        .Q(mem_read_data[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[20]),
        .Q(mem_read_data[20]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[21]),
        .Q(mem_read_data[21]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[22]),
        .Q(mem_read_data[22]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[23]),
        .Q(mem_read_data[23]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[24]),
        .Q(mem_read_data[24]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[25]),
        .Q(mem_read_data[25]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[26]),
        .Q(mem_read_data[26]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[27]),
        .Q(mem_read_data[27]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[28]),
        .Q(mem_read_data[28]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[29]),
        .Q(mem_read_data[29]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[2]),
        .Q(mem_read_data[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[30]),
        .Q(mem_read_data[30]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[31]),
        .Q(mem_read_data[31]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[3]),
        .Q(mem_read_data[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[4]),
        .Q(mem_read_data[4]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[5]),
        .Q(mem_read_data[5]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[6]),
        .Q(mem_read_data[6]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[7]),
        .Q(mem_read_data[7]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[8]),
        .Q(mem_read_data[8]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \axi_rdata_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(\axi_rdata[31]_i_2_n_0 ),
        .D(M_AXI_RDATA[9]),
        .Q(mem_read_data[9]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h28A8)) 
    axi_rready_i_1
       (.I0(M_AXI_ARESETN),
        .I1(M_AXI_RVALID),
        .I2(M_AXI_RREADY),
        .I3(M_AXI_RLAST),
        .O(axi_rready_i_1_n_0));
  FDRE axi_rready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_rready_i_1_n_0),
        .Q(M_AXI_RREADY),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h00F8)) 
    \axi_wdata[31]_i_1 
       (.I0(M_AXI_WVALID),
        .I1(M_AXI_WREADY),
        .I2(w_pluse_sync_reg_n_0),
        .I3(axi_wdata_ready_reg_n_0),
        .O(\axi_wdata[31]_i_1_n_0 ));
  FDRE axi_wdata_ready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_wdata[31]_i_1_n_0 ),
        .Q(axi_wdata_ready_reg_n_0),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[0]),
        .Q(M_AXI_WDATA[0]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[10]),
        .Q(M_AXI_WDATA[10]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[11]),
        .Q(M_AXI_WDATA[11]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[12]),
        .Q(M_AXI_WDATA[12]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[13]),
        .Q(M_AXI_WDATA[13]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[14]),
        .Q(M_AXI_WDATA[14]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[15]),
        .Q(M_AXI_WDATA[15]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[16]),
        .Q(M_AXI_WDATA[16]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[17]),
        .Q(M_AXI_WDATA[17]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[18]),
        .Q(M_AXI_WDATA[18]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[19]),
        .Q(M_AXI_WDATA[19]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[1]),
        .Q(M_AXI_WDATA[1]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[20]),
        .Q(M_AXI_WDATA[20]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[21]),
        .Q(M_AXI_WDATA[21]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[22]),
        .Q(M_AXI_WDATA[22]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[23]),
        .Q(M_AXI_WDATA[23]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[24]),
        .Q(M_AXI_WDATA[24]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[25]),
        .Q(M_AXI_WDATA[25]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[26]),
        .Q(M_AXI_WDATA[26]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[27]),
        .Q(M_AXI_WDATA[27]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[28]),
        .Q(M_AXI_WDATA[28]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[29]),
        .Q(M_AXI_WDATA[29]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[2]),
        .Q(M_AXI_WDATA[2]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[30]),
        .Q(M_AXI_WDATA[30]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[31]),
        .Q(M_AXI_WDATA[31]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[3]),
        .Q(M_AXI_WDATA[3]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[4]),
        .Q(M_AXI_WDATA[4]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[5]),
        .Q(M_AXI_WDATA[5]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[6]),
        .Q(M_AXI_WDATA[6]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[7]),
        .Q(M_AXI_WDATA[7]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[8]),
        .Q(M_AXI_WDATA[8]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDSE \axi_wdata_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(\axi_wdata[31]_i_1_n_0 ),
        .D(mem_write_data_reg[9]),
        .Q(M_AXI_WDATA[9]),
        .S(\axi_rdata[31]_i_1_n_0 ));
  FDRE axi_wlast_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(M_AXI_ARESETN),
        .Q(M_AXI_WLAST),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h3AFA)) 
    axi_wvalid_i_1
       (.I0(w_pluse_sync_reg_n_0),
        .I1(M_AXI_WLAST),
        .I2(M_AXI_WVALID),
        .I3(M_AXI_WREADY),
        .O(axi_wvalid_i_1_n_0));
  FDRE axi_wvalid_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_wvalid_i_1_n_0),
        .Q(M_AXI_WVALID),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hEA)) 
    mem_ack__0
       (.I0(axi_rdata_ready_reg_n_0),
        .I1(axi_wdata_ready_reg_n_0),
        .I2(M_AXI_WLAST),
        .O(mem_ack));
  FDRE \mem_addr_reg_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[8]),
        .Q(\mem_addr_reg_reg_n_0_[10] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[9]),
        .Q(\mem_addr_reg_reg_n_0_[11] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[10]),
        .Q(\mem_addr_reg_reg_n_0_[12] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[11]),
        .Q(\mem_addr_reg_reg_n_0_[13] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[12]),
        .Q(\mem_addr_reg_reg_n_0_[14] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[13]),
        .Q(\mem_addr_reg_reg_n_0_[15] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[14]),
        .Q(\mem_addr_reg_reg_n_0_[16] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[15]),
        .Q(\mem_addr_reg_reg_n_0_[17] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[16]),
        .Q(\mem_addr_reg_reg_n_0_[18] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[17]),
        .Q(\mem_addr_reg_reg_n_0_[19] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[18]),
        .Q(\mem_addr_reg_reg_n_0_[20] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[19]),
        .Q(\mem_addr_reg_reg_n_0_[21] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[20]),
        .Q(\mem_addr_reg_reg_n_0_[22] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[0]),
        .Q(\mem_addr_reg_reg_n_0_[2] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[1]),
        .Q(\mem_addr_reg_reg_n_0_[3] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[2]),
        .Q(\mem_addr_reg_reg_n_0_[4] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[3]),
        .Q(\mem_addr_reg_reg_n_0_[5] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[4]),
        .Q(\mem_addr_reg_reg_n_0_[6] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[5]),
        .Q(\mem_addr_reg_reg_n_0_[7] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[6]),
        .Q(\mem_addr_reg_reg_n_0_[8] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_addr_reg_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_addr[7]),
        .Q(\mem_addr_reg_reg_n_0_[9] ),
        .R(\axi_rdata[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h000E)) 
    \mem_byteenable_reg[3]_i_1 
       (.I0(dram_mem_write_en),
        .I1(dram_mem_read_en),
        .I2(r_pluse_sync_reg_n_0),
        .I3(w_pluse_sync_reg_n_0),
        .O(mem_byteenable_reg));
  FDRE \mem_byteenable_reg_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_byteenable[0]),
        .Q(M_AXI_WSTRB[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_byteenable_reg_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_byteenable[1]),
        .Q(M_AXI_WSTRB[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_byteenable_reg_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_byteenable[2]),
        .Q(M_AXI_WSTRB[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_byteenable_reg_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_byteenable[3]),
        .Q(M_AXI_WSTRB[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[0]),
        .Q(mem_write_data_reg[0]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[10]),
        .Q(mem_write_data_reg[10]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[11]),
        .Q(mem_write_data_reg[11]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[12]),
        .Q(mem_write_data_reg[12]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[13]),
        .Q(mem_write_data_reg[13]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[14]),
        .Q(mem_write_data_reg[14]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[15]),
        .Q(mem_write_data_reg[15]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[16]),
        .Q(mem_write_data_reg[16]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[17]),
        .Q(mem_write_data_reg[17]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[18]),
        .Q(mem_write_data_reg[18]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[19]),
        .Q(mem_write_data_reg[19]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[1]),
        .Q(mem_write_data_reg[1]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[20]),
        .Q(mem_write_data_reg[20]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[21]),
        .Q(mem_write_data_reg[21]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[22]),
        .Q(mem_write_data_reg[22]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[23]),
        .Q(mem_write_data_reg[23]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[24]),
        .Q(mem_write_data_reg[24]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[25]),
        .Q(mem_write_data_reg[25]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[26]),
        .Q(mem_write_data_reg[26]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[27]),
        .Q(mem_write_data_reg[27]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[28]),
        .Q(mem_write_data_reg[28]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[29]),
        .Q(mem_write_data_reg[29]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[2]),
        .Q(mem_write_data_reg[2]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[30]),
        .Q(mem_write_data_reg[30]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[31]),
        .Q(mem_write_data_reg[31]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[3]),
        .Q(mem_write_data_reg[3]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[4]),
        .Q(mem_write_data_reg[4]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[5]),
        .Q(mem_write_data_reg[5]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[6]),
        .Q(mem_write_data_reg[6]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[7]),
        .Q(mem_write_data_reg[7]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[8]),
        .Q(mem_write_data_reg[8]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  FDRE \mem_write_data_reg_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(mem_byteenable_reg),
        .D(mem_write_data[9]),
        .Q(mem_write_data_reg[9]),
        .R(\axi_rdata[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    r_pluse_sync_i_1
       (.I0(dram_mem_read_en),
        .I1(M_AXI_ARESETN),
        .I2(w_pluse_sync_reg_n_0),
        .I3(r_pluse_sync_reg_n_0),
        .O(r_pluse_sync_i_1_n_0));
  FDRE r_pluse_sync_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(r_pluse_sync_i_1_n_0),
        .Q(r_pluse_sync_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    w_pluse_sync_i_1
       (.I0(dram_mem_write_en),
        .I1(M_AXI_ARESETN),
        .I2(w_pluse_sync_reg_n_0),
        .I3(r_pluse_sync_reg_n_0),
        .O(w_pluse_sync_i_1_n_0));
  FDRE w_pluse_sync_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(w_pluse_sync_i_1_n_0),
        .Q(w_pluse_sync_reg_n_0),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
