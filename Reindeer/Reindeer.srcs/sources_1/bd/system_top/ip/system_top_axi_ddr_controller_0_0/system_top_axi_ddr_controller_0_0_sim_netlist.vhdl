-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Nov 23 16:08:16 2021
-- Host        : DESKTOP-65CBDF8 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_axi_ddr_controller_0_0/system_top_axi_ddr_controller_0_0_sim_netlist.vhdl
-- Design      : system_top_axi_ddr_controller_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_top_axi_ddr_controller_0_0_axi_ddr_controller is
  port (
    M_AXI_WVALID : out STD_LOGIC;
    M_AXI_RREADY : out STD_LOGIC;
    M_AXI_WLAST : out STD_LOGIC;
    mem_read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_AWADDR : out STD_LOGIC_VECTOR ( 21 downto 0 );
    M_AXI_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARADDR : out STD_LOGIC_VECTOR ( 21 downto 0 );
    mem_ack : out STD_LOGIC;
    M_AXI_BREADY : out STD_LOGIC;
    M_AXI_AWVALID : out STD_LOGIC;
    M_AXI_ARVALID : out STD_LOGIC;
    M_AXI_WREADY : in STD_LOGIC;
    M_AXI_RVALID : in STD_LOGIC;
    M_AXI_ACLK : in STD_LOGIC;
    M_AXI_ARESETN : in STD_LOGIC;
    M_AXI_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_addr : in STD_LOGIC_VECTOR ( 20 downto 0 );
    mem_write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_byteenable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    dram_mem_write_en : in STD_LOGIC;
    dram_mem_read_en : in STD_LOGIC;
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_BVALID : in STD_LOGIC;
    M_AXI_AWREADY : in STD_LOGIC;
    M_AXI_ARREADY : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_top_axi_ddr_controller_0_0_axi_ddr_controller : entity is "axi_ddr_controller";
end system_top_axi_ddr_controller_0_0_axi_ddr_controller;

architecture STRUCTURE of system_top_axi_ddr_controller_0_0_axi_ddr_controller is
  signal \M_AXI_ARADDR[19]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_n_0\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_n_1\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_n_2\ : STD_LOGIC;
  signal \M_AXI_ARADDR[19]_INST_0_n_3\ : STD_LOGIC;
  signal \^m_axi_arvalid\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_n_0\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_n_1\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_n_2\ : STD_LOGIC;
  signal \M_AXI_AWADDR[19]_INST_0_n_3\ : STD_LOGIC;
  signal \^m_axi_awvalid\ : STD_LOGIC;
  signal \^m_axi_bready\ : STD_LOGIC;
  signal \^m_axi_rready\ : STD_LOGIC;
  signal \^m_axi_wlast\ : STD_LOGIC;
  signal \^m_axi_wvalid\ : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 22 downto 19 );
  signal axi_arvalid_i_1_n_0 : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 22 downto 19 );
  signal axi_awvalid_i_1_n_0 : STD_LOGIC;
  signal axi_bready_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal axi_rdata_ready_reg_n_0 : STD_LOGIC;
  signal axi_rready_i_1_n_0 : STD_LOGIC;
  signal \axi_wdata[31]_i_1_n_0\ : STD_LOGIC;
  signal axi_wdata_ready_reg_n_0 : STD_LOGIC;
  signal axi_wvalid_i_1_n_0 : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[10]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[11]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[12]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[13]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[14]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[15]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[16]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[17]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[18]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[19]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[20]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[21]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[22]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[2]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[3]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[4]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[5]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[6]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[7]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[8]\ : STD_LOGIC;
  signal \mem_addr_reg_reg_n_0_[9]\ : STD_LOGIC;
  signal mem_byteenable_reg : STD_LOGIC;
  signal mem_write_data_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal r_pluse_sync_i_1_n_0 : STD_LOGIC;
  signal r_pluse_sync_reg_n_0 : STD_LOGIC;
  signal w_pluse_sync_i_1_n_0 : STD_LOGIC;
  signal w_pluse_sync_reg_n_0 : STD_LOGIC;
  signal \NLW_M_AXI_ARADDR[23]_INST_0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_M_AXI_ARADDR[23]_INST_0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXI_AWADDR[23]_INST_0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_M_AXI_AWADDR[23]_INST_0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of r_pluse_sync_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of w_pluse_sync_i_1 : label is "soft_lutpair0";
begin
  M_AXI_ARVALID <= \^m_axi_arvalid\;
  M_AXI_AWVALID <= \^m_axi_awvalid\;
  M_AXI_BREADY <= \^m_axi_bready\;
  M_AXI_RREADY <= \^m_axi_rready\;
  M_AXI_WLAST <= \^m_axi_wlast\;
  M_AXI_WVALID <= \^m_axi_wvalid\;
\M_AXI_ARADDR[19]_INST_0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \M_AXI_ARADDR[19]_INST_0_n_0\,
      CO(2) => \M_AXI_ARADDR[19]_INST_0_n_1\,
      CO(1) => \M_AXI_ARADDR[19]_INST_0_n_2\,
      CO(0) => \M_AXI_ARADDR[19]_INST_0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => axi_araddr(22 downto 20),
      DI(0) => '0',
      O(3 downto 0) => M_AXI_ARADDR(20 downto 17),
      S(3) => \M_AXI_ARADDR[19]_INST_0_i_1_n_0\,
      S(2) => \M_AXI_ARADDR[19]_INST_0_i_2_n_0\,
      S(1) => \M_AXI_ARADDR[19]_INST_0_i_3_n_0\,
      S(0) => axi_araddr(19)
    );
\M_AXI_ARADDR[19]_INST_0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_araddr(22),
      O => \M_AXI_ARADDR[19]_INST_0_i_1_n_0\
    );
\M_AXI_ARADDR[19]_INST_0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_araddr(21),
      O => \M_AXI_ARADDR[19]_INST_0_i_2_n_0\
    );
\M_AXI_ARADDR[19]_INST_0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_araddr(20),
      O => \M_AXI_ARADDR[19]_INST_0_i_3_n_0\
    );
\M_AXI_ARADDR[23]_INST_0\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXI_ARADDR[19]_INST_0_n_0\,
      CO(3 downto 1) => \NLW_M_AXI_ARADDR[23]_INST_0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => M_AXI_ARADDR(21),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXI_ARADDR[23]_INST_0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\M_AXI_AWADDR[19]_INST_0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \M_AXI_AWADDR[19]_INST_0_n_0\,
      CO(2) => \M_AXI_AWADDR[19]_INST_0_n_1\,
      CO(1) => \M_AXI_AWADDR[19]_INST_0_n_2\,
      CO(0) => \M_AXI_AWADDR[19]_INST_0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => axi_awaddr(22 downto 20),
      DI(0) => '0',
      O(3 downto 0) => M_AXI_AWADDR(20 downto 17),
      S(3) => \M_AXI_AWADDR[19]_INST_0_i_1_n_0\,
      S(2) => \M_AXI_AWADDR[19]_INST_0_i_2_n_0\,
      S(1) => \M_AXI_AWADDR[19]_INST_0_i_3_n_0\,
      S(0) => axi_awaddr(19)
    );
\M_AXI_AWADDR[19]_INST_0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_awaddr(22),
      O => \M_AXI_AWADDR[19]_INST_0_i_1_n_0\
    );
\M_AXI_AWADDR[19]_INST_0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_awaddr(21),
      O => \M_AXI_AWADDR[19]_INST_0_i_2_n_0\
    );
\M_AXI_AWADDR[19]_INST_0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => axi_awaddr(20),
      O => \M_AXI_AWADDR[19]_INST_0_i_3_n_0\
    );
\M_AXI_AWADDR[23]_INST_0\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXI_AWADDR[19]_INST_0_n_0\,
      CO(3 downto 1) => \NLW_M_AXI_AWADDR[23]_INST_0_CO_UNCONNECTED\(3 downto 1),
      CO(0) => M_AXI_AWADDR(21),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXI_AWADDR[23]_INST_0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\axi_araddr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[10]\,
      Q => M_AXI_ARADDR(8),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[11]\,
      Q => M_AXI_ARADDR(9),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[12]\,
      Q => M_AXI_ARADDR(10),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[13]\,
      Q => M_AXI_ARADDR(11),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[14]\,
      Q => M_AXI_ARADDR(12),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[15]\,
      Q => M_AXI_ARADDR(13),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[16]\,
      Q => M_AXI_ARADDR(14),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[17]\,
      Q => M_AXI_ARADDR(15),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[18]\,
      Q => M_AXI_ARADDR(16),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[19]\,
      Q => axi_araddr(19),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[20]\,
      Q => axi_araddr(20),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[21]\,
      Q => axi_araddr(21),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[22]\,
      Q => axi_araddr(22),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[2]\,
      Q => M_AXI_ARADDR(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[3]\,
      Q => M_AXI_ARADDR(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[4]\,
      Q => M_AXI_ARADDR(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[5]\,
      Q => M_AXI_ARADDR(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[6]\,
      Q => M_AXI_ARADDR(4),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[7]\,
      Q => M_AXI_ARADDR(5),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[8]\,
      Q => M_AXI_ARADDR(6),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_araddr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => r_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[9]\,
      Q => M_AXI_ARADDR(7),
      R => \axi_rdata[31]_i_1_n_0\
    );
axi_arvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => r_pluse_sync_reg_n_0,
      I1 => M_AXI_ARREADY,
      I2 => \^m_axi_arvalid\,
      O => axi_arvalid_i_1_n_0
    );
axi_arvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_arvalid_i_1_n_0,
      Q => \^m_axi_arvalid\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[10]\,
      Q => M_AXI_AWADDR(8),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[11]\,
      Q => M_AXI_AWADDR(9),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[12]\,
      Q => M_AXI_AWADDR(10),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[13]\,
      Q => M_AXI_AWADDR(11),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[14]\,
      Q => M_AXI_AWADDR(12),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[15]\,
      Q => M_AXI_AWADDR(13),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[16]\,
      Q => M_AXI_AWADDR(14),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[17]\,
      Q => M_AXI_AWADDR(15),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[18]\,
      Q => M_AXI_AWADDR(16),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[19]\,
      Q => axi_awaddr(19),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[20]\,
      Q => axi_awaddr(20),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[21]\,
      Q => axi_awaddr(21),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[22]\,
      Q => axi_awaddr(22),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[2]\,
      Q => M_AXI_AWADDR(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[3]\,
      Q => M_AXI_AWADDR(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[4]\,
      Q => M_AXI_AWADDR(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[5]\,
      Q => M_AXI_AWADDR(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[6]\,
      Q => M_AXI_AWADDR(4),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[7]\,
      Q => M_AXI_AWADDR(5),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[8]\,
      Q => M_AXI_AWADDR(6),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_awaddr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => w_pluse_sync_reg_n_0,
      D => \mem_addr_reg_reg_n_0_[9]\,
      Q => M_AXI_AWADDR(7),
      R => \axi_rdata[31]_i_1_n_0\
    );
axi_awvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => w_pluse_sync_reg_n_0,
      I1 => M_AXI_AWREADY,
      I2 => \^m_axi_awvalid\,
      O => axi_awvalid_i_1_n_0
    );
axi_awvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_awvalid_i_1_n_0,
      Q => \^m_axi_awvalid\,
      R => \axi_rdata[31]_i_1_n_0\
    );
axi_bready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \^m_axi_bready\,
      I1 => M_AXI_BVALID,
      I2 => M_AXI_ARESETN,
      I3 => w_pluse_sync_reg_n_0,
      O => axi_bready_i_1_n_0
    );
axi_bready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_bready_i_1_n_0,
      Q => \^m_axi_bready\,
      R => '0'
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => M_AXI_ARESETN,
      O => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => M_AXI_RVALID,
      I1 => \^m_axi_rready\,
      I2 => axi_rdata_ready_reg_n_0,
      O => \axi_rdata[31]_i_2_n_0\
    );
axi_rdata_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => \axi_rdata[31]_i_2_n_0\,
      Q => axi_rdata_ready_reg_n_0,
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(0),
      Q => mem_read_data(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(10),
      Q => mem_read_data(10),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(11),
      Q => mem_read_data(11),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(12),
      Q => mem_read_data(12),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(13),
      Q => mem_read_data(13),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(14),
      Q => mem_read_data(14),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(15),
      Q => mem_read_data(15),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(16),
      Q => mem_read_data(16),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(17),
      Q => mem_read_data(17),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(18),
      Q => mem_read_data(18),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(19),
      Q => mem_read_data(19),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(1),
      Q => mem_read_data(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(20),
      Q => mem_read_data(20),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(21),
      Q => mem_read_data(21),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(22),
      Q => mem_read_data(22),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(23),
      Q => mem_read_data(23),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(24),
      Q => mem_read_data(24),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(25),
      Q => mem_read_data(25),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(26),
      Q => mem_read_data(26),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(27),
      Q => mem_read_data(27),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(28),
      Q => mem_read_data(28),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(29),
      Q => mem_read_data(29),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(2),
      Q => mem_read_data(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(30),
      Q => mem_read_data(30),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(31),
      Q => mem_read_data(31),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(3),
      Q => mem_read_data(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(4),
      Q => mem_read_data(4),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(5),
      Q => mem_read_data(5),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(6),
      Q => mem_read_data(6),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(7),
      Q => mem_read_data(7),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(8),
      Q => mem_read_data(8),
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_rdata[31]_i_2_n_0\,
      D => M_AXI_RDATA(9),
      Q => mem_read_data(9),
      R => \axi_rdata[31]_i_1_n_0\
    );
axi_rready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"28A8"
    )
        port map (
      I0 => M_AXI_ARESETN,
      I1 => M_AXI_RVALID,
      I2 => \^m_axi_rready\,
      I3 => M_AXI_RLAST,
      O => axi_rready_i_1_n_0
    );
axi_rready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_rready_i_1_n_0,
      Q => \^m_axi_rready\,
      R => '0'
    );
\axi_wdata[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00F8"
    )
        port map (
      I0 => \^m_axi_wvalid\,
      I1 => M_AXI_WREADY,
      I2 => w_pluse_sync_reg_n_0,
      I3 => axi_wdata_ready_reg_n_0,
      O => \axi_wdata[31]_i_1_n_0\
    );
axi_wdata_ready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => \axi_wdata[31]_i_1_n_0\,
      Q => axi_wdata_ready_reg_n_0,
      R => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(0),
      Q => M_AXI_WDATA(0),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(10),
      Q => M_AXI_WDATA(10),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[11]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(11),
      Q => M_AXI_WDATA(11),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(12),
      Q => M_AXI_WDATA(12),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[13]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(13),
      Q => M_AXI_WDATA(13),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[14]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(14),
      Q => M_AXI_WDATA(14),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[15]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(15),
      Q => M_AXI_WDATA(15),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[16]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(16),
      Q => M_AXI_WDATA(16),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[17]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(17),
      Q => M_AXI_WDATA(17),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[18]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(18),
      Q => M_AXI_WDATA(18),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[19]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(19),
      Q => M_AXI_WDATA(19),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(1),
      Q => M_AXI_WDATA(1),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[20]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(20),
      Q => M_AXI_WDATA(20),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[21]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(21),
      Q => M_AXI_WDATA(21),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[22]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(22),
      Q => M_AXI_WDATA(22),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[23]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(23),
      Q => M_AXI_WDATA(23),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[24]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(24),
      Q => M_AXI_WDATA(24),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[25]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(25),
      Q => M_AXI_WDATA(25),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[26]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(26),
      Q => M_AXI_WDATA(26),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[27]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(27),
      Q => M_AXI_WDATA(27),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[28]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(28),
      Q => M_AXI_WDATA(28),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[29]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(29),
      Q => M_AXI_WDATA(29),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(2),
      Q => M_AXI_WDATA(2),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[30]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(30),
      Q => M_AXI_WDATA(30),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[31]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(31),
      Q => M_AXI_WDATA(31),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(3),
      Q => M_AXI_WDATA(3),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(4),
      Q => M_AXI_WDATA(4),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(5),
      Q => M_AXI_WDATA(5),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(6),
      Q => M_AXI_WDATA(6),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(7),
      Q => M_AXI_WDATA(7),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(8),
      Q => M_AXI_WDATA(8),
      S => \axi_rdata[31]_i_1_n_0\
    );
\axi_wdata_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => \axi_wdata[31]_i_1_n_0\,
      D => mem_write_data_reg(9),
      Q => M_AXI_WDATA(9),
      S => \axi_rdata[31]_i_1_n_0\
    );
axi_wlast_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => M_AXI_ARESETN,
      Q => \^m_axi_wlast\,
      R => '0'
    );
axi_wvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3AFA"
    )
        port map (
      I0 => w_pluse_sync_reg_n_0,
      I1 => \^m_axi_wlast\,
      I2 => \^m_axi_wvalid\,
      I3 => M_AXI_WREADY,
      O => axi_wvalid_i_1_n_0
    );
axi_wvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_wvalid_i_1_n_0,
      Q => \^m_axi_wvalid\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_ack__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => axi_rdata_ready_reg_n_0,
      I1 => axi_wdata_ready_reg_n_0,
      I2 => \^m_axi_wlast\,
      O => mem_ack
    );
\mem_addr_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(8),
      Q => \mem_addr_reg_reg_n_0_[10]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(9),
      Q => \mem_addr_reg_reg_n_0_[11]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(10),
      Q => \mem_addr_reg_reg_n_0_[12]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(11),
      Q => \mem_addr_reg_reg_n_0_[13]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(12),
      Q => \mem_addr_reg_reg_n_0_[14]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(13),
      Q => \mem_addr_reg_reg_n_0_[15]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(14),
      Q => \mem_addr_reg_reg_n_0_[16]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(15),
      Q => \mem_addr_reg_reg_n_0_[17]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(16),
      Q => \mem_addr_reg_reg_n_0_[18]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(17),
      Q => \mem_addr_reg_reg_n_0_[19]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(18),
      Q => \mem_addr_reg_reg_n_0_[20]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(19),
      Q => \mem_addr_reg_reg_n_0_[21]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(20),
      Q => \mem_addr_reg_reg_n_0_[22]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(0),
      Q => \mem_addr_reg_reg_n_0_[2]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(1),
      Q => \mem_addr_reg_reg_n_0_[3]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(2),
      Q => \mem_addr_reg_reg_n_0_[4]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(3),
      Q => \mem_addr_reg_reg_n_0_[5]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(4),
      Q => \mem_addr_reg_reg_n_0_[6]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(5),
      Q => \mem_addr_reg_reg_n_0_[7]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(6),
      Q => \mem_addr_reg_reg_n_0_[8]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_addr_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_addr(7),
      Q => \mem_addr_reg_reg_n_0_[9]\,
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_byteenable_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000E"
    )
        port map (
      I0 => dram_mem_write_en,
      I1 => dram_mem_read_en,
      I2 => r_pluse_sync_reg_n_0,
      I3 => w_pluse_sync_reg_n_0,
      O => mem_byteenable_reg
    );
\mem_byteenable_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_byteenable(0),
      Q => M_AXI_WSTRB(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_byteenable_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_byteenable(1),
      Q => M_AXI_WSTRB(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_byteenable_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_byteenable(2),
      Q => M_AXI_WSTRB(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_byteenable_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_byteenable(3),
      Q => M_AXI_WSTRB(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(0),
      Q => mem_write_data_reg(0),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(10),
      Q => mem_write_data_reg(10),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(11),
      Q => mem_write_data_reg(11),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(12),
      Q => mem_write_data_reg(12),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(13),
      Q => mem_write_data_reg(13),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(14),
      Q => mem_write_data_reg(14),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(15),
      Q => mem_write_data_reg(15),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(16),
      Q => mem_write_data_reg(16),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(17),
      Q => mem_write_data_reg(17),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(18),
      Q => mem_write_data_reg(18),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(19),
      Q => mem_write_data_reg(19),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(1),
      Q => mem_write_data_reg(1),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(20),
      Q => mem_write_data_reg(20),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(21),
      Q => mem_write_data_reg(21),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(22),
      Q => mem_write_data_reg(22),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(23),
      Q => mem_write_data_reg(23),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(24),
      Q => mem_write_data_reg(24),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(25),
      Q => mem_write_data_reg(25),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(26),
      Q => mem_write_data_reg(26),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(27),
      Q => mem_write_data_reg(27),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(28),
      Q => mem_write_data_reg(28),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(29),
      Q => mem_write_data_reg(29),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(2),
      Q => mem_write_data_reg(2),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(30),
      Q => mem_write_data_reg(30),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(31),
      Q => mem_write_data_reg(31),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(3),
      Q => mem_write_data_reg(3),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(4),
      Q => mem_write_data_reg(4),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(5),
      Q => mem_write_data_reg(5),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(6),
      Q => mem_write_data_reg(6),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(7),
      Q => mem_write_data_reg(7),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(8),
      Q => mem_write_data_reg(8),
      R => \axi_rdata[31]_i_1_n_0\
    );
\mem_write_data_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => mem_byteenable_reg,
      D => mem_write_data(9),
      Q => mem_write_data_reg(9),
      R => \axi_rdata[31]_i_1_n_0\
    );
r_pluse_sync_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => dram_mem_read_en,
      I1 => M_AXI_ARESETN,
      I2 => w_pluse_sync_reg_n_0,
      I3 => r_pluse_sync_reg_n_0,
      O => r_pluse_sync_i_1_n_0
    );
r_pluse_sync_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => r_pluse_sync_i_1_n_0,
      Q => r_pluse_sync_reg_n_0,
      R => '0'
    );
w_pluse_sync_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => dram_mem_write_en,
      I1 => M_AXI_ARESETN,
      I2 => w_pluse_sync_reg_n_0,
      I3 => r_pluse_sync_reg_n_0,
      O => w_pluse_sync_i_1_n_0
    );
w_pluse_sync_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => w_pluse_sync_i_1_n_0,
      Q => w_pluse_sync_reg_n_0,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_top_axi_ddr_controller_0_0 is
  port (
    dram_mem_read_en : in STD_LOGIC;
    dram_mem_write_en : in STD_LOGIC;
    mem_byteenable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_addr : in STD_LOGIC_VECTOR ( 20 downto 0 );
    mem_write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_ack : out STD_LOGIC;
    mem_read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_ACLK : in STD_LOGIC;
    M_AXI_ARESETN : in STD_LOGIC;
    M_AXI_AWID : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_AWLOCK : out STD_LOGIC;
    M_AXI_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_AWUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_AWVALID : out STD_LOGIC;
    M_AXI_AWREADY : in STD_LOGIC;
    M_AXI_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_WLAST : out STD_LOGIC;
    M_AXI_WUSER : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_WVALID : out STD_LOGIC;
    M_AXI_WREADY : in STD_LOGIC;
    M_AXI_BID : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_BUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_BVALID : in STD_LOGIC;
    M_AXI_BREADY : out STD_LOGIC;
    M_AXI_ARID : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_ARLOCK : out STD_LOGIC;
    M_AXI_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_ARVALID : out STD_LOGIC;
    M_AXI_ARREADY : in STD_LOGIC;
    M_AXI_RID : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_RUSER : in STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_RVALID : in STD_LOGIC;
    M_AXI_RREADY : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_top_axi_ddr_controller_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_top_axi_ddr_controller_0_0 : entity is "system_top_axi_ddr_controller_0_0,axi_ddr_controller,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_top_axi_ddr_controller_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of system_top_axi_ddr_controller_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_top_axi_ddr_controller_0_0 : entity is "axi_ddr_controller,Vivado 2019.1";
end system_top_axi_ddr_controller_0_0;

architecture STRUCTURE of system_top_axi_ddr_controller_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m_axi_araddr\ : STD_LOGIC_VECTOR ( 23 downto 2 );
  signal \^m_axi_awaddr\ : STD_LOGIC_VECTOR ( 23 downto 2 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of M_AXI_ACLK : signal is "xilinx.com:signal:clock:1.0 M_AXI_ACLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of M_AXI_ACLK : signal is "XIL_INTERFACENAME M_AXI_ACLK, ASSOCIATED_BUSIF M_AXI, ASSOCIATED_RESET M_AXI_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN system_top_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_ARESETN : signal is "xilinx.com:signal:reset:1.0 M_AXI_ARESETN RST";
  attribute X_INTERFACE_PARAMETER of M_AXI_ARESETN : signal is "XIL_INTERFACENAME M_AXI_ARESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_ARLOCK : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARLOCK";
  attribute X_INTERFACE_INFO of M_AXI_ARREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARREADY";
  attribute X_INTERFACE_INFO of M_AXI_ARVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARVALID";
  attribute X_INTERFACE_INFO of M_AXI_AWLOCK : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWLOCK";
  attribute X_INTERFACE_INFO of M_AXI_AWREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWREADY";
  attribute X_INTERFACE_INFO of M_AXI_AWVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWVALID";
  attribute X_INTERFACE_INFO of M_AXI_BREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI BREADY";
  attribute X_INTERFACE_INFO of M_AXI_BVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI BVALID";
  attribute X_INTERFACE_INFO of M_AXI_RLAST : signal is "xilinx.com:interface:aximm:1.0 M_AXI RLAST";
  attribute X_INTERFACE_INFO of M_AXI_RREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of M_AXI_RREADY : signal is "XIL_INTERFACENAME M_AXI, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 50000000, ID_WIDTH 1, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 4, RUSER_WIDTH 4, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN system_top_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_RVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI RVALID";
  attribute X_INTERFACE_INFO of M_AXI_WLAST : signal is "xilinx.com:interface:aximm:1.0 M_AXI WLAST";
  attribute X_INTERFACE_INFO of M_AXI_WREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI WREADY";
  attribute X_INTERFACE_INFO of M_AXI_WVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI WVALID";
  attribute X_INTERFACE_INFO of M_AXI_ARADDR : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARADDR";
  attribute X_INTERFACE_INFO of M_AXI_ARBURST : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARBURST";
  attribute X_INTERFACE_INFO of M_AXI_ARCACHE : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARCACHE";
  attribute X_INTERFACE_INFO of M_AXI_ARID : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARID";
  attribute X_INTERFACE_INFO of M_AXI_ARLEN : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARLEN";
  attribute X_INTERFACE_INFO of M_AXI_ARPROT : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARPROT";
  attribute X_INTERFACE_INFO of M_AXI_ARQOS : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARQOS";
  attribute X_INTERFACE_INFO of M_AXI_ARSIZE : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARSIZE";
  attribute X_INTERFACE_INFO of M_AXI_ARUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARUSER";
  attribute X_INTERFACE_INFO of M_AXI_AWADDR : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWADDR";
  attribute X_INTERFACE_INFO of M_AXI_AWBURST : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWBURST";
  attribute X_INTERFACE_INFO of M_AXI_AWCACHE : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWCACHE";
  attribute X_INTERFACE_INFO of M_AXI_AWID : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWID";
  attribute X_INTERFACE_INFO of M_AXI_AWLEN : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWLEN";
  attribute X_INTERFACE_INFO of M_AXI_AWPROT : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWPROT";
  attribute X_INTERFACE_INFO of M_AXI_AWQOS : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWQOS";
  attribute X_INTERFACE_INFO of M_AXI_AWSIZE : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWSIZE";
  attribute X_INTERFACE_INFO of M_AXI_AWUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWUSER";
  attribute X_INTERFACE_INFO of M_AXI_BID : signal is "xilinx.com:interface:aximm:1.0 M_AXI BID";
  attribute X_INTERFACE_INFO of M_AXI_BRESP : signal is "xilinx.com:interface:aximm:1.0 M_AXI BRESP";
  attribute X_INTERFACE_INFO of M_AXI_BUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI BUSER";
  attribute X_INTERFACE_INFO of M_AXI_RDATA : signal is "xilinx.com:interface:aximm:1.0 M_AXI RDATA";
  attribute X_INTERFACE_INFO of M_AXI_RID : signal is "xilinx.com:interface:aximm:1.0 M_AXI RID";
  attribute X_INTERFACE_INFO of M_AXI_RRESP : signal is "xilinx.com:interface:aximm:1.0 M_AXI RRESP";
  attribute X_INTERFACE_INFO of M_AXI_RUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI RUSER";
  attribute X_INTERFACE_INFO of M_AXI_WDATA : signal is "xilinx.com:interface:aximm:1.0 M_AXI WDATA";
  attribute X_INTERFACE_INFO of M_AXI_WSTRB : signal is "xilinx.com:interface:aximm:1.0 M_AXI WSTRB";
  attribute X_INTERFACE_INFO of M_AXI_WUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI WUSER";
begin
  M_AXI_ARADDR(31) <= \<const0>\;
  M_AXI_ARADDR(30) <= \<const0>\;
  M_AXI_ARADDR(29) <= \<const0>\;
  M_AXI_ARADDR(28) <= \<const1>\;
  M_AXI_ARADDR(27) <= \<const1>\;
  M_AXI_ARADDR(26) <= \<const1>\;
  M_AXI_ARADDR(25) <= \<const1>\;
  M_AXI_ARADDR(24) <= \<const1>\;
  M_AXI_ARADDR(23 downto 2) <= \^m_axi_araddr\(23 downto 2);
  M_AXI_ARADDR(1) <= \<const0>\;
  M_AXI_ARADDR(0) <= \<const0>\;
  M_AXI_ARBURST(1) <= \<const0>\;
  M_AXI_ARBURST(0) <= \<const1>\;
  M_AXI_ARCACHE(3) <= \<const0>\;
  M_AXI_ARCACHE(2) <= \<const0>\;
  M_AXI_ARCACHE(1) <= \<const1>\;
  M_AXI_ARCACHE(0) <= \<const0>\;
  M_AXI_ARID(0) <= \<const0>\;
  M_AXI_ARLEN(7) <= \<const0>\;
  M_AXI_ARLEN(6) <= \<const0>\;
  M_AXI_ARLEN(5) <= \<const0>\;
  M_AXI_ARLEN(4) <= \<const0>\;
  M_AXI_ARLEN(3) <= \<const0>\;
  M_AXI_ARLEN(2) <= \<const0>\;
  M_AXI_ARLEN(1) <= \<const0>\;
  M_AXI_ARLEN(0) <= \<const0>\;
  M_AXI_ARLOCK <= \<const0>\;
  M_AXI_ARPROT(2) <= \<const0>\;
  M_AXI_ARPROT(1) <= \<const0>\;
  M_AXI_ARPROT(0) <= \<const0>\;
  M_AXI_ARQOS(3) <= \<const0>\;
  M_AXI_ARQOS(2) <= \<const0>\;
  M_AXI_ARQOS(1) <= \<const0>\;
  M_AXI_ARQOS(0) <= \<const0>\;
  M_AXI_ARSIZE(2) <= \<const0>\;
  M_AXI_ARSIZE(1) <= \<const1>\;
  M_AXI_ARSIZE(0) <= \<const0>\;
  M_AXI_ARUSER(0) <= \<const1>\;
  M_AXI_AWADDR(31) <= \<const0>\;
  M_AXI_AWADDR(30) <= \<const0>\;
  M_AXI_AWADDR(29) <= \<const0>\;
  M_AXI_AWADDR(28) <= \<const1>\;
  M_AXI_AWADDR(27) <= \<const1>\;
  M_AXI_AWADDR(26) <= \<const1>\;
  M_AXI_AWADDR(25) <= \<const1>\;
  M_AXI_AWADDR(24) <= \<const1>\;
  M_AXI_AWADDR(23 downto 2) <= \^m_axi_awaddr\(23 downto 2);
  M_AXI_AWADDR(1) <= \<const0>\;
  M_AXI_AWADDR(0) <= \<const0>\;
  M_AXI_AWBURST(1) <= \<const0>\;
  M_AXI_AWBURST(0) <= \<const1>\;
  M_AXI_AWCACHE(3) <= \<const0>\;
  M_AXI_AWCACHE(2) <= \<const0>\;
  M_AXI_AWCACHE(1) <= \<const1>\;
  M_AXI_AWCACHE(0) <= \<const0>\;
  M_AXI_AWID(0) <= \<const0>\;
  M_AXI_AWLEN(7) <= \<const0>\;
  M_AXI_AWLEN(6) <= \<const0>\;
  M_AXI_AWLEN(5) <= \<const0>\;
  M_AXI_AWLEN(4) <= \<const0>\;
  M_AXI_AWLEN(3) <= \<const0>\;
  M_AXI_AWLEN(2) <= \<const0>\;
  M_AXI_AWLEN(1) <= \<const0>\;
  M_AXI_AWLEN(0) <= \<const0>\;
  M_AXI_AWLOCK <= \<const0>\;
  M_AXI_AWPROT(2) <= \<const0>\;
  M_AXI_AWPROT(1) <= \<const0>\;
  M_AXI_AWPROT(0) <= \<const0>\;
  M_AXI_AWQOS(3) <= \<const0>\;
  M_AXI_AWQOS(2) <= \<const0>\;
  M_AXI_AWQOS(1) <= \<const0>\;
  M_AXI_AWQOS(0) <= \<const0>\;
  M_AXI_AWSIZE(2) <= \<const0>\;
  M_AXI_AWSIZE(1) <= \<const1>\;
  M_AXI_AWSIZE(0) <= \<const0>\;
  M_AXI_AWUSER(0) <= \<const1>\;
  M_AXI_WUSER(3) <= \<const0>\;
  M_AXI_WUSER(2) <= \<const0>\;
  M_AXI_WUSER(1) <= \<const0>\;
  M_AXI_WUSER(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.system_top_axi_ddr_controller_0_0_axi_ddr_controller
     port map (
      M_AXI_ACLK => M_AXI_ACLK,
      M_AXI_ARADDR(21 downto 0) => \^m_axi_araddr\(23 downto 2),
      M_AXI_ARESETN => M_AXI_ARESETN,
      M_AXI_ARREADY => M_AXI_ARREADY,
      M_AXI_ARVALID => M_AXI_ARVALID,
      M_AXI_AWADDR(21 downto 0) => \^m_axi_awaddr\(23 downto 2),
      M_AXI_AWREADY => M_AXI_AWREADY,
      M_AXI_AWVALID => M_AXI_AWVALID,
      M_AXI_BREADY => M_AXI_BREADY,
      M_AXI_BVALID => M_AXI_BVALID,
      M_AXI_RDATA(31 downto 0) => M_AXI_RDATA(31 downto 0),
      M_AXI_RLAST => M_AXI_RLAST,
      M_AXI_RREADY => M_AXI_RREADY,
      M_AXI_RVALID => M_AXI_RVALID,
      M_AXI_WDATA(31 downto 0) => M_AXI_WDATA(31 downto 0),
      M_AXI_WLAST => M_AXI_WLAST,
      M_AXI_WREADY => M_AXI_WREADY,
      M_AXI_WSTRB(3 downto 0) => M_AXI_WSTRB(3 downto 0),
      M_AXI_WVALID => M_AXI_WVALID,
      dram_mem_read_en => dram_mem_read_en,
      dram_mem_write_en => dram_mem_write_en,
      mem_ack => mem_ack,
      mem_addr(20 downto 0) => mem_addr(20 downto 0),
      mem_byteenable(3 downto 0) => mem_byteenable(3 downto 0),
      mem_read_data(31 downto 0) => mem_read_data(31 downto 0),
      mem_write_data(31 downto 0) => mem_write_data(31 downto 0)
    );
end STRUCTURE;
