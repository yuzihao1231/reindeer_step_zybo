// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Nov 29 13:04:34 2021
// Host        : DESKTOP-65CBDF8 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_system_ila_0_0/system_top_system_ila_0_0_stub.v
// Design      : system_top_system_ila_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "bd_7c07,Vivado 2019.1" *)
module system_top_system_ila_0_0(clk, probe0, probe1, probe2, probe3, probe4, probe5, 
  probe6, probe7, probe8, probe9)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[0:0],probe1[0:0],probe2[0:0],probe3[3:0],probe4[20:0],probe5[31:0],probe6[0:0],probe7[31:0],probe8[0:0],probe9[0:0]" */;
  input clk;
  input [0:0]probe0;
  input [0:0]probe1;
  input [0:0]probe2;
  input [3:0]probe3;
  input [20:0]probe4;
  input [31:0]probe5;
  input [0:0]probe6;
  input [31:0]probe7;
  input [0:0]probe8;
  input [0:0]probe9;
endmodule
