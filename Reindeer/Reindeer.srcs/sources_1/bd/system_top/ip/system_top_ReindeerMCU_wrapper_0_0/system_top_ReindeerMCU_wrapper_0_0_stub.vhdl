-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Nov 29 12:50:18 2021
-- Host        : DESKTOP-65CBDF8 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_ReindeerMCU_wrapper_0_0/system_top_ReindeerMCU_wrapper_0_0_stub.vhdl
-- Design      : system_top_ReindeerMCU_wrapper_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_top_ReindeerMCU_wrapper_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    sync_reset : in STD_LOGIC;
    start : in STD_LOGIC;
    start_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
    cpu_suspending : in STD_LOGIC;
    processor_paused : out STD_LOGIC;
    dram_mem_read_en : out STD_LOGIC;
    dram_mem_write_en : out STD_LOGIC;
    mem_byteenable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_addr : out STD_LOGIC_VECTOR ( 20 downto 0 );
    mem_write_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    mem_ack : in STD_LOGIC;
    mem_read_data : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end system_top_ReindeerMCU_wrapper_0_0;

architecture stub of system_top_ReindeerMCU_wrapper_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,reset_n,sync_reset,start,start_address[31:0],cpu_suspending,processor_paused,dram_mem_read_en,dram_mem_write_en,mem_byteenable[3:0],mem_addr[20:0],mem_write_data[31:0],mem_ack,mem_read_data[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ReindeerMCU_wrapper,Vivado 2019.1";
begin
end;
