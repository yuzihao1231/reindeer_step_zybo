// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Nov 29 12:50:18 2021
// Host        : DESKTOP-65CBDF8 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/YZH/RISC-V/Reindeer_zybo/Reindeer/Reindeer.srcs/sources_1/bd/system_top/ip/system_top_ReindeerMCU_wrapper_0_0/system_top_ReindeerMCU_wrapper_0_0_stub.v
// Design      : system_top_ReindeerMCU_wrapper_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ReindeerMCU_wrapper,Vivado 2019.1" *)
module system_top_ReindeerMCU_wrapper_0_0(clk, reset_n, sync_reset, start, start_address, 
  cpu_suspending, processor_paused, dram_mem_read_en, dram_mem_write_en, mem_byteenable, 
  mem_addr, mem_write_data, mem_ack, mem_read_data)
/* synthesis syn_black_box black_box_pad_pin="clk,reset_n,sync_reset,start,start_address[31:0],cpu_suspending,processor_paused,dram_mem_read_en,dram_mem_write_en,mem_byteenable[3:0],mem_addr[20:0],mem_write_data[31:0],mem_ack,mem_read_data[31:0]" */;
  input clk;
  input reset_n;
  input sync_reset;
  input start;
  input [31:0]start_address;
  input cpu_suspending;
  output processor_paused;
  output dram_mem_read_en;
  output dram_mem_write_en;
  output [3:0]mem_byteenable;
  output [20:0]mem_addr;
  output [31:0]mem_write_data;
  input mem_ack;
  input [31:0]mem_read_data;
endmodule
