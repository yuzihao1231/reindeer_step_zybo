`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/19 18:19:49
// Design Name: 
// Module Name: ReindeerMCU_wrapper
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "sdram_controller.vh"
`include "common.vh"

module ReindeerMCU_wrapper(
    input   wire                                         clk,                          
    input   wire                                         reset_n,                      
    input   wire                                         sync_reset,       
    
    //cpu control
    input   wire                                            start,
    //31:23是CPU总线地址，应当为0x100, 1:0应当为0用于字节对齐，22:2为memery地址范围
    input   wire  [`PC_BITWIDTH - 1 : 0]                    start_address,//32'h80000000
    input   wire                                            cpu_suspending,//阻塞信号
    output  wire                                            processor_paused,
    
    //mem interface
    output wire                                          dram_mem_read_en,
    output wire                                          dram_mem_write_en,
    output wire [`SDRAM_DATA_BITS/*32*/ / 8 - 1 : 0]      mem_byteenable,//字节有效位
    output wire [`SDRAM_ADDR_BITS/*21*/ - 1 : 0]          mem_addr,//dram_mem_addr // address for 32 bit word
    output wire [`SDRAM_DATA_BITS - 1 : 0]                mem_write_data,
    input wire                                           mem_ack,//读写成功后的ack信号
    input wire [`SDRAM_DATA_BITS - 1 : 0]                 mem_read_data
    );
    
wire GND;
assign GND = 1'b0;

PulseRain_Reindeer_MCU PulseRain_Reindeer_MCU_i (
    .clk (clk),
    .reset_n (reset_n), //active low
    .sync_reset (sync_reset),
    
    .INTx ({`NUM_OF_INTx{1'b0}}),//int0 <= ~(&five_way_keys_debounced);

    .ocd_read_enable (1'b0),
    .ocd_write_enable (1'b0),
    .ocd_rw_addr (21'b0),
    .ocd_write_word (32'b0),
    .ocd_mem_enable_out (),
    .ocd_mem_word_out (),        //assign ocd_mem_word_out = mem_read_data;
    .ocd_reg_read_addr (5'd2),
    .ocd_reg_we (1'b0),
    .ocd_reg_write_addr (5'd2),
    .ocd_reg_write_data (`DEFAULT_STACK_ADDR),

//    .RXD (RXD),
//    .TXD (uart_tx_cpu),
    
//    .GPIO_IN (gpio_in),
//    .GPIO_OUT(gpio_out),
    
//    .sda_in (ADXL345_SDA),
//    .scl_in (ADXL345_SCL),
    
//    .sda_out (sda_out),
//    .scl_out (scl_out),

    .start (start),
    .start_address (start_address),

    .suspend (cpu_suspending),
    .processor_paused (processor_paused),

//  mem interface
    .dram_ack             (mem_ack),
    .dram_mem_read_data   (mem_read_data),
    .dram_mem_addr        (mem_addr),
    .dram_mem_read_en     (dram_mem_read_en),
    .dram_mem_write_en    (dram_mem_write_en),
    .dram_mem_byte_enable (mem_byteenable),
    .dram_mem_write_data  (mem_write_data),

    .peek_pc (),
    .peek_ir (),
    .peek_mem_write_en   (),
    .peek_mem_write_data (),
    .peek_mem_addr       ());

endmodule
