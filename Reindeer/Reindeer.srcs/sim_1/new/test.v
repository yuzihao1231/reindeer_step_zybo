`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/11/15 13:40:04
// Design Name: 
// Module Name: test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test();
reg [2:0] data_sp;
reg [15:0] ad_value[7:0];
reg [15:0] ad_value_data;

reg clock;
initial begin
clock = 0;
data_sp = 4'b0;
ad_value_data = 16'd11;

ad_value[4'd0] = 16'd12;
#10
ad_value[4'd1] = 16'd5;
#10
ad_value[4'd10] = 16'd6;
#10
repeat(30) #10
  begin
    clock <= ~clock;
  end
$stop;
end

always@(posedge clock) begin
    ad_value[data_sp] <= ad_value_data;
    ad_value_data <= ad_value_data + 16'b1;
    data_sp <= data_sp + 4'b1;
end

endmodule
